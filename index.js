// alert("Classes")
// Class
	// blueprint that describes an object in a non-specific way

	// Why Use Classes?
		// Reusable codes


	// In JS, classes can be created using "class" keyword and {}
	/*
			Syntax:

			class <NameOfClass> {
		
			}
	*/



// MiniActivity1

class Person {
	constructor(name,age,nationality,address){
		this.name = name;
		if(typeof age === "number" && age >=18){
			this.age =age;
		} else{
			this.age = undefined;
		}

		this.nationality = nationality;
		this.address = address;
	}
}


let personOne = new Person("Jane", 23, "Filipino", "Manila, PH")
let personTwo = new Person("Craig", 18, "American", "New York, USA")



// Activity #1

// Quiz
/*
1. Class
2. Pascal Casing
3. new
4. Declaration/Instantiation
5. constructor
*/

// Function Coding

//create student one
let studentOneName = 'John';
let studentOneEmail = 'john@mail.com';
let studentOneGrades = [89, 84, 78, 88];

//create student two
let studentTwoName = 'Joe';
let studentTwoEmail = 'joe@mail.com';
let studentTwoGrades = [78, 82, 79, 85];

//create student three
let studentThreeName = 'Jane';
let studentThreeEmail = 'jane@mail.com';
let studentThreeGrades = [87, 89, 91, 93];

//create student four
let studentFourName = 'Jessie';
let studentFourEmail = 'jessie@mail.com';
let studentFourGrades = [91, 89, 92, 93];



class Student {
	// constructor is to add properties for our class 
	constructor(name, email, grades){
		// key : value/parameter
		this.name = name;
		this.email = email;
		this.gradeAve = undefined;
		this.passed = undefined;
		this.passedWithHonors = undefined;


		if(grades.length ===4){
			if(grades.every(grade => grade >= 0 && grade <= 100)){
				this.grades = grades
			}
			else {
				this.grades = undefined
			}
		}
		else {
			this.grades = undefined
		}

	}

	// Start of creating Class Methods
	login(){
		console.log(`${this.email} has logged in`)
		// console.log(this)
		return this;
				// to return an object
	}

	logout(){
		console.log(`${this.email} has logged out`)
		return this;

	}

	listGrades(){
		console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`)
		return this;
	}


	computeAve(){
		let sum = 0
		this.grades.forEach(grade => sum = sum + grade);
		// return sum/4;
		// update property
		this.gradeAve = sum/4
		// return object
		return this;
	}


	willPass(){
		this.passed = this.computeAve() >=85 ? true : false;
		return this;
	}


	willPassWithHonors(){
		if (this.passed){
			if(this.computeAve() >=90){
				this.passedWithHonors = true
			}else {
				this.passedWithHonors = false;
			}
		}
		else {
				this.passedWithHonors = false;
			}
		return this;
	}



}


// Activity #2
	// Quiz
	/*
	1.No
	2.No
	3.Yes
	4. Dot Notation
	5. return this
	*/

// Function Coding

// studentOne.login().computeAve().willPass().willPassWithHonors().logout()




// Important to use keyword "new" to create a new object from classes
// The "new" keyword will look for the constructor method in class being instantiated
let studentOne = new Student("John","john@mail.com",[89, 84, 78, 88]);
let studentTwo = new Student("Joe","joe@mail.com",[78, 82, 79, 85]);
let studentThree = new Student("Jane","jane@mail.com",[87, 89, 91, 93]);
let studentFour = new Student("Jessie","jessie@mail.com",[91, 89, 92, 93]);


let studentFive = new Student('Cris','cris@mail.com',[101, 90, 91, 92]);
let studentSix = new Student('Shein','shein@mail.com',[-1, 75, 89, 90]);



	// How to change value of key properties?
		// studentFour.email = "jess@mail.com"


// Class Methods
	// Methods are define OUTSIDE the constructor
	// NOT SEPARATED by comma


// Getter and Setter
	// Best practice dictates that we regulate access to such properties
	// Getter - Retrieval
	// Setter - Manipulation

// Chain of Methods
	// return this




